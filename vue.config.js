module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
        ? '/registrador/'
        : '/',

    "transpileDependencies": [
        "vuetify"
    ]
}
