import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    activaciones: []
  },
  mutations: {
    agregarActivacion(state, numero){
      const aux = state.activaciones.filter(e=>e.numero===numero)
      if(aux.length===0){
        const id = Date.now()
        state.activaciones.push({numero, activado:false, id})
      }
    },
    eliminarActivacion(state, objeto){
      const i = state.activaciones.indexOf(objeto)
      confirm('¿Eliminar número?') && state.activaciones.splice(i, 1)
    },
    activar(state, objeto){
      const index = state.activaciones.indexOf(objeto)
      // if (!state.activaciones[index].activado)
      const ultimosDigitos = (state.activaciones[index].numero+"").substr(-9)
      confirm('¿Enviar mensaje para activarlo?') && (state.activaciones[index].activado=true && window.open(`sms:475?body=28787*${ultimosDigitos}*41232`, '_blank'))

    },
  },
  actions: {
  },
  modules: {
  }
})
